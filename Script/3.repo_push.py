#!/usr/bin/env python
# -*- coding: utf-8 -*-
#0.1.0
import os
import sys
import os.path

def main():

    # 打印当前路径
    current_path = os.getcwd()
    print('======================================================================')
    print('✅ 当前所在路径：' + current_path)

    
    # repo根目录
    repo_root_path = sys.argv[1]
    print('✅ repo根目录：' + repo_root_path)


    # 切换到repo根目录
    os.chdir(repo_root_path)


    # 找到.podspec.json文件
    podspec_json_file = func_get_podspec_json_file(repo_root_path)
    podspec_json_file_path = repo_root_path + '/' + podspec_json_file
    if os.path.exists(podspec_json_file_path):
        print('✅ .podspec.json文件路径：' + podspec_json_file_path)
    else:
        print('❌ 未找到.podspec.json文件')
        exit(1)


    # 更新repo
    binary_repos_name = 'zcp-repos-binary'
    cocoapods_specs_source = 'https://github.com/CocoaPods/Specs.git'
    zcp_repos_source = 'https://gitlab.com/Malygos/zcp-repos.git'
    os.chdir(repo_root_path)
    os.system('pod repo push ' + binary_repos_name + ' ' + podspec_json_file + ' --sources=\'' + cocoapods_specs_source + ',' + zcp_repos_source + '\' --verbose --allow-warnings')
    os.chdir(current_path)

    
    # 删除.podspec.json文件
    os.remove(podspec_json_file_path)
    if os.path.exists(podspec_json_file_path):
        print('❌ ' + podspec_json_file + '文件删除失败')
    else:
        print('✅ 已删除' + podspec_json_file)

# ====================================================================================================
# Function
# ====================================================================================================

# 获取当前podspec文件
def func_get_podspec_json_file(repo_root_path):
    json_file = ''
    for file in os.listdir(repo_root_path):
        if file.endswith('.podspec.json'):
            json_file = file
            break
    return json_file


if __name__ == '__main__':
    main()