#!/usr/bin/env python
# -*- coding: utf-8 -*-
#0.1.0
import os
import sys
import os.path

def main():

    # 打印当前路径
    current_path = os.getcwd()
    print('======================================================================')
    print('✅ 当前所在路径：' + current_path)

    
    # repo根目录
    repo_root_path = sys.argv[1]
    print('✅ repo根目录：' + repo_root_path)


    # 切换到repo跟目录
    os.chdir(repo_root_path)

    # 当前仓库最新commit id
    commit_id = os.popen('git rev-list --all --max-count=1').readlines()[0].replace('\n', '')
    print('✅ 当前repo的最新commit id：' + commit_id)


    # 找到podspec文件
    podspec_file = func_get_current_podspec_file(repo_root_path)
    podspec_file_name = podspec_file.split('.')[0]
    podspec_file_path = repo_root_path + '/' + podspec_file
    if os.path.exists(podspec_file_path):
        print('✅ podspec文件路径：' + podspec_file_path)
    else:
        print('❌ 未找到podspec文件')
        exit(1)


    # 根据当前podspec文件生成framework.podspec
    framework_podspec_file_path = func_generate_framework_podspec_file(repo_root_path, podspec_file_path, commit_id)
    if os.path.exists(framework_podspec_file_path):
        print('✅ 已生成framework.podspec')
    else:
        print('❌ framework.podspec文件生成失败')
        exit(1)

    # 根据framework.podspec文件生成json文件
    framework_podspec_json_file_path = func_generate_framework_podspec_json_file(repo_root_path, podspec_file_name)
    if os.path.exists(framework_podspec_json_file_path):
        print('✅ 已生成' + podspec_file_name + '.podspec.json')
    else:
        print('❌ ' + podspec_file_name + '.podspec.json文件生成失败')
        exit(1)

    # 删除framework.podspec
    os.remove(framework_podspec_file_path)
    if os.path.exists(framework_podspec_file_path):
        print('❌ framework.podspec文件删除失败')
    else:
        print('✅ 已删除framework.podspec')

# ====================================================================================================
# Function
# ====================================================================================================

# 获取当前podspec文件
def func_get_current_podspec_file(repo_root_path):
    podspec_file = ''
    for file in os.listdir(repo_root_path):
        if file.endswith('.podspec') and file != 'framework.podspec':
            podspec_file = file
            break
    return podspec_file

# 生成framework.podspec
def func_generate_framework_podspec_file(repo_root_path, repo_podspec_file_path, commit_id):
    # 读取podspec文件
    read_file_Handle = open(repo_podspec_file_path)
    podspec_contents = read_file_Handle.read()
    read_file_Handle.close()

    # 解析podspec文件
    podspec_contents_list = podspec_contents.split('\n')
    framework_podspec_contents = ''

    flag_source = 0
    flag_framework = 0
    if_prefix_blank = ''
    for podspec_row in podspec_contents_list:
        if 'IS_SOURCE' in podspec_row:
            flag_source = 1
            if_prefix_blank = podspec_row.split('if')[0]
            continue
        if 'IS_FRAMEWORK' in podspec_row:
            flag_framework = 1
            if_prefix_blank = podspec_row.split('if')[0]
            continue
        if 'end' in podspec_row and (podspec_row.split('end')[0] == if_prefix_blank):
            if flag_source:
                flag_source = 0
            if flag_framework:
                flag_framework = 0
            continue
        if flag_source:
            continue

        if '.version' in podspec_row and (not '.source' in podspec_row):
            podspec_row = podspec_row.split('=')[0] + '= \"' + podspec_row.split('\"')[1] + '-SNAPSHOT\"'
        if '.source' in podspec_row:
            podspec_row = podspec_row.split(':tag')[0] + ':commit => \'' + commit_id + '\' }'

        framework_podspec_contents += podspec_row + '\n'

    # 生成framework.podspec文件
    framework_podspec_file_path = repo_root_path + '/' + 'framework.podspec'
    write_file_Handle = open(framework_podspec_file_path, 'w')
    write_file_Handle.write(framework_podspec_contents)
    write_file_Handle.close()

    return framework_podspec_file_path

# 生成json文件
def func_generate_framework_podspec_json_file(repo_root_path, podspec_file_name):
    # json文件路径
    json_file_path = repo_root_path + '/' + podspec_file_name + '.podspec.json'

    # 若json文件存在则删除
    if os.path.exists(json_file_path):
        os.remove(json_file_path)

    # 生成json文件
    current_path = os.getcwd()
    os.chdir(repo_root_path)
    os.system('pod ipc spec framework.podspec >> ' + podspec_file_name + '.podspec.json')
    os.chdir(current_path)

    return json_file_path

if __name__ == '__main__':
    main()