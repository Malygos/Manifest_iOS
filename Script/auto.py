#!/usr/bin/env python
# -*- coding: utf-8 -*-
#0.1.0
import os
import sys
import os.path

def main():

    # 判断repo库根路径
    repo_root_path = os.getcwd()
    if len(sys.argv) >= 2:
        repo_root_path = sys.argv[1]

    if not func_repo_root_path_avaliable(repo_root_path):
        print('❌ 当前不是一个有效的根路径，因为该路径下无podspec文件 ' + repo_root_path)
        exit(1)

    # 执行的python文件所在的路径
    python_file_path = sys.argv[0]
    python_file = python_file_path.split('/').pop()
    python_dir = python_file_path.replace(python_file, '')

    # 1.更新SNAPSHOT tag
    code1 = os.system('sh ' + python_dir + '1.post-receive ' + repo_root_path)
    if code1 != 0:
        exit(1)

    # 2.生成podspec.json文件
    code2 = os.system('python ' + python_dir + '2.create_podspec_json.py ' + repo_root_path)
    if code2 != 0:
    	exit(1)

    # 3、推送到二进制repos中
    code3 = os.system('python ' + python_dir + '3.repo_push.py ' + repo_root_path)
    if code3 != 0:
        exit(1)


def func_repo_root_path_avaliable(repo_root_path):
    # 读取该路径下的podspec文件
    podspec_file = ''
    for file in os.listdir(repo_root_path):
        if file.endswith('.podspec') and file != 'framework.podspec':
            podspec_file = file
            break

    # 判断该路径下是否有podspec文件，若有则是一个正确的根路径
    podspec_file_path = repo_root_path + '/' + podspec_file
    if len(podspec_file) > 0 and os.path.exists(podspec_file_path):
        return 1
    else:
        return 0


if __name__ == '__main__':
    main()